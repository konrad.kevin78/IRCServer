#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#ifndef USER_H
#define USER_H

typedef struct _User {
    char* name;
    char* old_name;
    char* real_name;
    char* hostname;
    char* away_msg;
    int id;
    int mode;
    int socket;
    pthread_t thread;
    pthread_mutex_t socket_mutex;
} User;

typedef struct _NodeUser {
    struct _NodeUser* next;
    User* user;
} NodeUser;

typedef struct _ListUsers {
    NodeUser* first;
    NodeUser* last;
    unsigned int size;
} ListUsers;

User* new_user(char* name, char* real_name, char* hostname, int mode, int socket);
ListUsers* new_list_users();
NodeUser* new_node_user(User* user);
void add_user(ListUsers* list, User* user);
void remove_user(ListUsers* list, User* user);
User* get_user_by_name(char* name);
char* get_user_description(User* user);
char* user_get_channels_names(User* user);

#endif
