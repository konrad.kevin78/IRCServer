#ifndef RECEIVE_COMMANDS_H
#define RECEIVE_COMMANDS_H

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>

void send_numeric_reply(char* numeric, User* target, char* message);
void receive_nick(User *, char **);
void receive_user(User *, char **, char *);
void receive_ping(User *);
void receive_list(User *, char **);
void receive_part(User *, char **, char *);
void receive_mode(User *, char **);
void receive_topic(User *, char **, char *);
void receive_names(User *, char **);
void receive_invite(User *, char **);
void receive_motd(User *);
void receive_time(User *);
void receive_users(User *);
void receive_away(User *, char *);
void receive_whois(User*, char **);
void receive_who(User *, char **);
void receive_join(User*, char **);
void receive_privmsg(User *, char **, char *);
void receive_quit(User *, char *);
void receive_unknown(User *, char *);

void receive_kick(User *, char **, char *);
void receive_notice(User *, char **, char *);
void receive_ison(User *, char **);
void receive_pong(User *);

#endif
