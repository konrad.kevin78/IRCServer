#include "../include/user.h"
#include "../include/util.h"
#include "../include/receive_commands.h"
#include "../include/messages.h"
#include "../include/connect_user.h"


void send_numeric_reply(char* numeric, User* target, char* message) {
    char* result = strset(":");
    result = stradd(result, server_hostname);
    result = stradd(result, " ");
    result = stradd(result, numeric);
    result = stradd(result, " ");
    result = stradd(result, target->name);
    result = stradd(result, " ");
    result = stradd(result, message);
    result = stradd(result, "\n");

    write(target->socket, result, strlen(result));
}

void receive_nick(User* user, char** args) {
    char* nick = args[0];
    nick = strtok(nick, " \t\r\n");

    if (nick == NULL) {
        send_numeric_reply(ERR_NONICKNAMEGIVEN, user, ":No nickname given");
        return;
    }

    if (get_user_by_name(nick) != NULL) {
        send_numeric_reply(ERR_NICKNAMEINUSE, user, ":Nickname is already in use");
        return;
    }

    user->old_name = user->name;
    user->name = nick;

    if (strcmp(user->old_name, "*") != 0) {
        char* message;
        message = strset(":");
        message = stradd(message, user->old_name);
        message = stradd(message, "!");
        message = stradd(message, user->old_name);
        message = stradd(message, "@");
        message = stradd(message, user->hostname);
        message = stradd(message, " ");
        message = stradd(message, NICK);
        message = stradd(message, " ");
        message = stradd(message, user->name);
        message = stradd(message, "\n");

        write(user->socket, message, strlen(message));
        broadcast_channels(user, message);
    }
}

void receive_user(User *user, char** args, char* real_name) {
    char* nick = args[0];
    char* mode = args[1];
    char* unused = args[2];
    char* p_converter;

    if (nick == NULL || mode == NULL || unused == NULL || real_name == NULL) {
        send_numeric_reply(ERR_NEEDMOREPARAMS, user, "USER :Not enough parameters");
        return;
    }

    real_name = strtok(real_name, "\r\n");

    user->old_name = malloc(strlen(user->name));
    user->real_name = malloc(strlen(real_name));
    user->mode = (int) strtol(mode, &p_converter, 10);
    strcpy(user->real_name, real_name);
    strcpy(user->old_name, user->name);
    strcpy(user->name, nick);

    char* message = ":Welcome to the Internet Relay Network ";
    message = stradd(message, get_user_description(user));
    send_numeric_reply(RPL_WELCOME, user, message);
}

void receive_mode(User* user, char** args) {
    char* username = args[0];
    char* mode = args[1];
    char* p_converter;

    if (username == NULL || mode == NULL) {
        send_numeric_reply(ERR_NEEDMOREPARAMS, user, "MODE :Not enough parameters");
        return;
    }

    if (strcmp(username, user->name) != 0) {
        send_numeric_reply(ERR_USERSDONTMATCH, user, ":Cannot change mode for other users");
        return;
    }

    user->mode = (int) strtol(mode, &p_converter, 10);
    send_numeric_reply(RPL_UMODEIS, user, ":Mode changed");
}

void receive_topic(User* user, char** args, char* message) {
    char* channel_name = args[0];

    if (channel_name == NULL) {
        send_numeric_reply(ERR_NEEDMOREPARAMS, user, "TOPIC :Not enough parameters");
        return;
    }

    channel_name = strtok(channel_name, " #,\r\n");
    Channel* channel = channel_get_by_name(channel_name);

    if (channel == NULL) {
        char* result = strset(channel_name);
        result = stradd(result, " :You're not on channel");
        send_numeric_reply(ERR_NOTONCHANNEL, user, result);
        return;
    }

    if (channel_contains_user(user, channel) == -1) {
        char* result = strset(channel_name);
        result = stradd(result, " :You're not on channel");
        send_numeric_reply(ERR_NOTONCHANNEL, user, result);
        return;
    }

    if (message == NULL) {
        send_channel_topic(user, channel);
        return;
    }

    message = strtok(message, "\r\n");
    channel_update_topic(user, channel, message);
}

void receive_ping(User *user) {
    char* message;
    message = strset("PONG ");
    message = stradd(message, server_hostname);
    message = stradd(message, " ");
    message = stradd(message, user->hostname);
    message = stradd(message, "\n");
    write(user->socket, message, strlen(message));
}

void receive_whois(User* user, char** args) {
    char* nick = args[0];

    if (nick == NULL) {
        send_numeric_reply(ERR_NONICKNAMEGIVEN, user, ":No nickname given");
        return;
    }

    User* target = get_user_by_name(nick);
    if (target == NULL) {
        char* message;
        message = strset(nick);
        message = stradd(message, " :No such nick");
        send_numeric_reply(ERR_NOSUCHNICK, user, message);
        return;
    }

    char* who_is_user;
    who_is_user = strset(target->name);
    who_is_user = stradd(who_is_user, " ");
    who_is_user = stradd(who_is_user, target->name);
    who_is_user = stradd(who_is_user, " ");
    who_is_user = stradd(who_is_user, target->hostname);
    who_is_user = stradd(who_is_user, " * :");
    who_is_user = stradd(who_is_user, target->real_name);
    send_numeric_reply(RPL_WHOISUSER, user, who_is_user);

    char* who_is_server = strset(target->name);
    who_is_server = stradd(who_is_server, " ");
    who_is_server = stradd(who_is_server, server_hostname);
    who_is_server = stradd(who_is_server, " :IRC server from ESGI 4A MOC");
    send_numeric_reply(RPL_WHOISSERVER, user, who_is_server);

    char* who_is_operator = strset(target->name);
    who_is_operator = stradd(who_is_operator, " :is an IRC operator");
    send_numeric_reply(RPL_WHOISOPERATOR, user, who_is_operator);

    char* who_is_idle = strset(target->name);
    who_is_idle = stradd(who_is_idle, " 0 :seconds idle");
    send_numeric_reply(RPL_WHOISIDLE, user, who_is_idle);

    char* who_is_channels = strset(target->name);
    char* channels = user_get_channels_names(user);
    who_is_channels = stradd(who_is_channels, " :");
    who_is_channels = stradd(who_is_channels, channels);
    send_numeric_reply(RPL_WHOISCHANNELS, user, who_is_channels);

    char* who_is_end;
    who_is_end = strset(target->name);
    who_is_end = stradd(who_is_end, " :End of WHOIS list");
    send_numeric_reply(RPL_ENDOFWHOIS, user, who_is_end);
}

void receive_away(User* user, char* message) {
    if (message == NULL) {
        user->away_msg = NULL;
        send_numeric_reply(RPL_UNAWAY, user, " :You are no longer marked as being away");
    } else {
        user->away_msg = message;
        send_numeric_reply(RPL_NOWAWAY, user, " :You have been marked as being away");
    }
}

void receive_users(User* user) {
    send_numeric_reply(ERR_USERSDISABLED, user, " :USERS has been disabled");
}

void receive_invite(User* user, char** args) {
    char* target_name = args[0];
    char* channels_name = args[1];

    if (target_name == NULL || channels_name == NULL) {
        send_numeric_reply(ERR_NEEDMOREPARAMS, user, "INVITE :Not enough parameters");
        return;
    }

    User* target = get_user_by_name(target_name);
    if (target == NULL) {
        char* message = strset(target_name);
        message = stradd(message, " :No such nick");
        send_numeric_reply(ERR_NOSUCHNICK, user, message);
        return;
    }

    char* channel_name = strtok(channels_name, " #,\r\n");
    while (channel_name != NULL) {
        Channel* channel = channel_get_by_name(channel_name);

        if (channel == NULL) {
            char* message = strset(channel_name);
            message = stradd(message, " :You're not on that channel");
            send_numeric_reply(ERR_NOTONCHANNEL, user, message);
            channel_name = strtok(NULL, " #,\r\n");
            continue;
        }

        if (channel_contains_user(user, channel) == -1) {
            char* message = strset(channel_name);
            message = stradd(message, " :You're not on that channel");
            send_numeric_reply(ERR_NOTONCHANNEL, user, message);
            channel_name = strtok(NULL, " #,\r\n");
            continue;
        }

        char* invite_reply = strset(channel->name);
        invite_reply = stradd(invite_reply, " ");
        invite_reply = stradd(invite_reply, target->name);
        send_numeric_reply(RPL_INVITING, user, invite_reply);

        char* message = strset(":");
        message = stradd(message, get_user_description(user));
        message = stradd(message, " ");
        message = stradd(message, INVITE);
        message = stradd(message, " ");
        message = stradd(message, target->name);
        message = stradd(message, " #");
        message = stradd(message, channel->name);
        message = stradd(message, "\n");

        write(user->socket, message, strlen(message));
        write(target->socket, message, strlen(message));
        channel_name = strtok(NULL, " #,\r\n");
    }
}

void receive_motd(User* user) {
    if (motd == NULL) {
        send_numeric_reply(ERR_NOMOTD, user, ":MOTD File is missing");
        return;
    }

    char* start_motd = strset(":- ");
    start_motd = stradd(start_motd, server_hostname);
    start_motd = stradd(start_motd, " Message of the day - ");
    send_numeric_reply(RPL_MOTDSTART, user, start_motd);

    char* text_motd = strset(":- ");
    text_motd = stradd(text_motd, motd);
    send_numeric_reply(RPL_MOTD, user, text_motd);

    char* end_motd = strset(":End of MOTD command");
    send_numeric_reply(RPL_MOTD, user, end_motd);
}

void receive_time(User* user) {
    char time_message[100];
    time_t now = time(NULL);
    struct tm* t = localtime(&now);
    strftime(time_message, sizeof(time_message), "%d/%m/%Y %H:%M:%S", t);

    char* result = strset(server_hostname);
    result = stradd(result, " :");
    result = stradd(result, time_message);
    send_numeric_reply(RPL_TIME, user, result);
}

void receive_quit(User *user, char* message) {
    channel_leave_all(user, message);

    pthread_mutex_lock(&user->socket_mutex);
    close(user->socket);
    pthread_mutex_unlock(&user->socket_mutex);

    pthread_mutex_lock(&users_mutex);
    remove_user(users, user);
    pthread_mutex_unlock(&users_mutex);
}

void receive_privmsg(User* user, char** args, char* message) {
    char* recipient = args[0];

    if (recipient == NULL) {
        send_numeric_reply(ERR_NORECIPIENT, user, ":No recipient given (PRIVMSG)");
        return;
    }

    if (message == NULL) {
        send_numeric_reply(ERR_NOTEXTTOSEND, user, ":No text to send");
        return;
    }

    message = strtok(message, "\r\n\t");
    recipient = strtok(recipient, " ,#\r\n");
    Channel* channel = channel_get_by_name(recipient);
    User* target = get_user_by_name(recipient);

    if (channel == NULL && target == NULL) {
        char* result = strset(recipient);
        result = stradd(result, " :No such nick/channel");
        send_numeric_reply(ERR_NOSUCHNICK, user, result);
        return;
    }

    if (channel != NULL) {
        char* result = strset(":");
        result = stradd(result, get_user_description(user));
        result = stradd(result, " ");
        result = stradd(result, PRIVMSG);
        result = stradd(result, " #");
        result = stradd(result, channel->name);
        result = stradd(result, " :");
        result = stradd(result, message);
        result = stradd(result, "\n");

        channel_send_all(user, channel, result, 0);
    } else if (target != NULL) {
        if (target->away_msg == NULL) {
            char* result = strset(":");
            result = stradd(result, get_user_description(user));
            result = stradd(result, " ");
            result = stradd(result, PRIVMSG);
            result = stradd(result, " ");
            result = stradd(result, target->name);
            result = stradd(result, " :");
            result = stradd(result, message);
            result = stradd(result, "\n");

            write(target->socket, result, strlen(result));
        } else {
            char* result = strset(target->name);
            result = stradd(result, " :");
            result = stradd(result, target->away_msg);
            send_numeric_reply(RPL_AWAY, user, result);
        }
    }
}

void receive_join(User* user, char** args) {
    char* channels_string = args[0];

    if (channels == NULL) {
        send_numeric_reply(ERR_NEEDMOREPARAMS, user, "JOIN :Not enough parameters");
        return;
    }

    char* channel_name = strtok(channels_string, " #,\r\n");

    if (strcmp(channel_name, "0") == 0) {
        channel_leave_all(user, "");
        return;
    }

    while (channel_name != NULL) {
        Channel* channel = channel_get_by_name(channel_name);

        if (channel == NULL) {
            char* message = strset(channel_name);
            message = stradd(message, " :No such channel");
            send_numeric_reply(ERR_NOSUCHCHANNEL, user, message);
            channel_name = strtok(NULL, " #\r\n");
            continue;
        }

        if (channel_contains_user(user, channel) != -1) {
            char* message = strset(channel_name);
            message = stradd(message, " :Already joined this channel");
            send_numeric_reply(ERR_TOOMANYCHANNELS, user, message);
            channel_name = strtok(NULL, " #,\r\n");
            continue;
        }

        if (channel->users->size >= channel->max_users) {
            char* message = strset(channel_name);
            message = stradd(message, " :Cannot join channel (+l)");
            send_numeric_reply(ERR_NOSUCHCHANNEL, user, message);
            channel_name = strtok(NULL, " #,\r\n");
            continue;
        }

        channel_join(user, channel);
        channel_name = strtok(NULL, " #,\r\n");
    }
}

void receive_names(User* user, char** args) {
    char* channels_string = args[0];

    if (channels_string == NULL) {
        NodeChannel* node = channels->first;
        while (node != NULL) {
            send_channel_names(user, node->channel);
            node = node->next;
        }
    } else {
        char* channel_name = strtok(channels_string, " #,\r\n");
        while (channel_name != NULL) {
            Channel* channel = channel_get_by_name(channel_name);

            if (channel == NULL) {
                char* result = strset(channel_name);
                result = stradd(result, " :No such channel");
                send_numeric_reply(ERR_NOSUCHCHANNEL, user, result);
                channel_name = strtok(NULL, " #,\r\n");
                continue;
            }

            send_channel_names(user, channel);
            channel_name = strtok(NULL, " #,\r\n");
        }
    }
}

void receive_part(User *user, char** args, char* message) {
    char* channels_string = args[0];

    if (channels == NULL) {
        send_numeric_reply(ERR_NEEDMOREPARAMS, user, "PART :Not enough parameters");
        return;
    }

    if (strcmp(channels_string, "0") == 0) {
        channel_leave(user, NULL, NULL);
        return;
    }

    if (message != NULL) {
        message = strtok(message, "\r\n");
    }

    char* channel_name = strtok(channels_string, " #,\r\n");
    while (channel_name != NULL) {
        Channel* channel = channel_get_by_name(channel_name);

        if (channel == NULL) {
            char* result = strset(channel_name);
            result = stradd(result, " :No such channel");
            send_numeric_reply(ERR_NOSUCHCHANNEL, user, result);
            channel_name = strtok(NULL, " #,\r\n");
            continue;
        }

        if (channel_contains_user(user, channel) == -1) {
            char* result = strset(channel_name);
            result = stradd(result, " :You're not on that channel");
            send_numeric_reply(ERR_NOTONCHANNEL, user, result);
            channel_name = strtok(NULL, " #,\r\n");
            continue;
        }

        channel_leave(user, channel, message);
        channel_name = strtok(NULL, " #,\r\n");
    }
}

void receive_list(User* user, char** args) {
    // TODO : user args to match requested channels only
    send_numeric_reply(RPL_LISTSTART, user, "Channel :Users Name");

    NodeChannel* node = channels->first;
    while (node != NULL) {
        char* description = channel_get_description(node->channel);
        send_numeric_reply(RPL_LIST, user, description);
        node = node->next;
    }

    send_numeric_reply(RPL_LISTEND, user, ":End of LIST");
}

void receive_unknown(User* user, char* command) {
    if (command == NULL) {
        command = "";
    }

    command = strtok(command, "\r\n");

    char* message = strset(command);
    message = stradd(message, " :Unknown command");
    send_numeric_reply(ERR_UNKNOWNCOMMAND, user, message);
}

void receive_kick(User* user, char** args, char* message) {
    // TODO receive_kick
}

void receive_who(User* user, char** args) {
    // TODO : receive_who
}

void receive_notice(User* user, char** args, char* message) {
    // TODO : receive_notice
}

void receive_ison(User* user, char** args) {
    // TODO : receive_ison
}

void receive_pong(User* user) {
    // TODO : receive_pong
}