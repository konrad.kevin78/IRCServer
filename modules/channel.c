#include "../include/channel.h"
#include "../include/connect_user.h"

Channel* new_channel(char* name, char* topic, int max_users) {
    Channel* channel = malloc(sizeof(Channel));

    channel->name = malloc(strlen(name));

    if (topic == NULL) {
        channel->topic = NULL;
    } else {
        channel->topic = malloc(strlen(topic));
        strcpy(channel->topic, topic);
    }

    channel->users = new_list_users();
    channel->max_users = max_users;
    strcpy(channel->name, name);

    return channel;
}

char* channel_get_description(Channel* channel) {
    char* description;
    char users_count_str[4];
    sprintf(users_count_str, "%d", channel->users->size);

    description = strset(channel->name);
    description = stradd(description, " ");
    description = stradd(description, users_count_str);
    description = stradd(description, " :");

    if (channel->topic != NULL) {
        description = stradd(description, channel->topic);
    }

    return description;
}

ListChannels* new_list_channels() {
    ListChannels* list = malloc(sizeof(ListChannels));
    list->first = NULL;
    list->last = NULL;
    list->size = 0;
    return list;
}

NodeChannel* new_node_channel(Channel* channel) {
    NodeChannel* node = malloc(sizeof(NodeChannel));
    node->channel = channel;
    node->next = NULL;
    return node;
}

void add_channel(ListChannels* list, Channel* channel) {
    NodeChannel* node = new_node_channel(channel);

    if (list->last == NULL) {
        list->first = node;
    } else {
        list->last->next = node;
    }

    list->last = node;
    list->size += 1;
}

void broadcast_channels(User* user, char* message) {
    NodeChannel* node = channels->first;

    while (node != NULL) {
        if (channel_contains_user(user, node->channel) != -1) {
            channel_send_all(user, node->channel, message, 1);
        }

        node = node->next;
    }
}

Channel* channel_get_by_name(char* channel_name) {
    NodeChannel* node = channels->first;

    while (node != NULL) {
        if (strcmp(node->channel->name, channel_name) == 0) {
            return node->channel;
        }

        node = node->next;
    }

    return NULL;
}

void channel_join(User* user, Channel* channel) {
    pthread_mutex_lock(&channels_mutex);
    add_user(channel->users, user);
    pthread_mutex_unlock(&channels_mutex);

    char* result = strset(":");
    result = stradd(result, get_user_description(user));
    result = stradd(result, " ");
    result = stradd(result, JOIN);
    result = stradd(result, " #");
    result = stradd(result, channel->name);
    result = stradd(result, "\n");

    channel_send_all(user, channel, result, 1);
    send_channel_topic(user, channel);
    send_channel_names(user, channel);
}

void send_channel_names(User* user, Channel* channel) {
    char* names;
    names = strset(" = #");
    names = stradd(names, channel->name);
    names = stradd(names, " :");
    NodeUser* node = channel->users->first;

    while (node != NULL) {
        names = stradd(names, node->user->name);

        if (node->next != NULL) {
            names = stradd(names, " ");
        }

        node = node->next;
    }

    send_numeric_reply(RPL_NAMREPLY, user, names);

    char* end_of_names = strset(channel->name);
    end_of_names = stradd(end_of_names, " :End of NAMES list");
    send_numeric_reply(RPL_ENDOFNAMES, user, end_of_names);
}

void send_channel_topic(User* user, Channel* channel) {
    if (channel->topic == NULL) {
        char* topic_reply = strset(channel->name);
        topic_reply = stradd(topic_reply, " :No topic is set");
        send_numeric_reply(RPL_NOTOPIC, user, topic_reply);
    } else {
        char* topic_reply = strset(channel->name);
        topic_reply = stradd(topic_reply, " :");
        topic_reply = stradd(topic_reply, channel->topic);
        send_numeric_reply(RPL_TOPIC, user, topic_reply);
    }
}

void channel_update_topic(User* user, Channel* channel, char* topic) {
    if (topic == NULL) {
        channel->topic = NULL;
    } else {
        channel->topic = topic;
    }

    char* message = strset(":");
    message = stradd(message, get_user_description(user));
    message = stradd(message, " ");
    message = stradd(message, TOPIC);
    message = stradd(message, " #");
    message = stradd(message, channel->name);
    message = stradd(message, " :");

    if (channel->topic == NULL) {
        message = stradd(message, "");
    } else {
        message = stradd(message, channel->topic);
    }

    message = stradd(message, "\n");
    channel_send_all(user, channel, message, 1);
}

void channel_leave_all(User* user, char* message) {
    NodeChannel* node = channels->first;

    while (node != NULL) {
        if (channel_contains_user(user, node->channel) != -1) {
            channel_leave(user, node->channel, message);
        }

        node = node->next;
    }
}

void channel_leave(User* user, Channel* channel, char* message) {
    pthread_mutex_lock(&channels_mutex);
    remove_user(channel->users, user);
    pthread_mutex_unlock(&channels_mutex);

    char* result = strset(":");
    result = stradd(result, get_user_description(user));
    result = stradd(result, " ");
    result = stradd(result, PART);
    result = stradd(result, " #");
    result = stradd(result, channel->name);

    if (message != NULL) {
        result = stradd(result, " :");
        result = stradd(result, message);
    }

    result = stradd(result, "\n");

    channel_send_all(user, channel, result, 1);
    write(user->socket, result, strlen(result));
}

void channel_send_all(User* user, Channel* channel, char* message, unsigned short include_sender) {
    ListUsers* users = channel->users;
    NodeUser* node = users->first;

    while (node != NULL) {
        if (node->user->id != user->id || include_sender == 1) {
            write(node->user->socket, message, strlen(message));
        }

        node = node->next;
    }
}

int channel_contains_user(User* user, Channel* channel) {
    int position = 0;
    ListUsers* users = channel->users;
    NodeUser* node = users->first;

    while (node != NULL) {
        if (node->user->id == user->id) {
            return position;
        }

        node = node->next;
        position++;
    }

    return -1;
}