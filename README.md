[![Build Status](https://travis-ci.org/Nerimah/IRCServer.svg?branch=master)](https://travis-ci.org/Nerimah/IRCServer)

###Compilation
cmake .

###Available commands
- nick
- user
- ping
- list
- part
- mode
- topic
- names
- invite
- motd
- time
- users
- away
- whois
- join
- privmsg
- quit

###Missing commands
- kick
- notice
- ison
- pong
- who