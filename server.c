#include "include/connect_user.h"

int main(int argc, char **argv) {
    int server_socket;
    int user_socket;
    struct sockaddr_in server_address;
    struct sockaddr_in client_address;
    socklen_t client_address_size = sizeof(client_address);

    if (argc != 2) {
        fprintf(stderr, "Usage: %s [port]\n", argv[0]);
        exit(1);
    }

    if ((server_socket = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        fprintf(stderr, "[Error creating socket: %s\n", strerror(errno));
        exit(2);
    }

    char* p_converter;
    int port = (int) strtol(argv[1], &p_converter, 10);
    bzero(&server_address, sizeof(server_address));
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_port = htons(port);

    if (bind(server_socket, (struct sockaddr *) &server_address, sizeof(server_address)) == -1) {
        fprintf(stderr, "[Error binding server: %s]\n", strerror(errno));
        exit(3);
    }

    if (listen(server_socket, LISTENQ) == -1) {
        fprintf(stderr, "[Error listening socket: %s]\n", strerror(errno));
        exit(4);
    }

    motd = "This is the message of the day!";
    server_hostname = malloc(1024 * sizeof(char));
    gethostname(server_hostname, 1024);

    users = new_list_users();
    channels = new_list_channels();

    pthread_mutex_init(&users_mutex, NULL);
    pthread_mutex_init(&channels_mutex, NULL);

    Channel* channel1 = new_channel("test", NULL, 2);
    Channel* channel2 = new_channel("hello_world", "Stop by to say hello to the world", 5);
    Channel* channel3 = new_channel("main", "This is the main channel", 20);

    pthread_mutex_lock(&channels_mutex);
    add_channel(channels, channel1);
    add_channel(channels, channel2);
    add_channel(channels, channel3);
    pthread_mutex_unlock(&channels_mutex);

    printf("[Server initialized on port %s.]\n", argv[1]);
    printf("[Press CTRL+C to exit.]\n");

    while (1) {
        if ((user_socket = accept(server_socket, (struct sockaddr *) &client_address, &client_address_size)) == -1 ) {
            fprintf(stderr, "[Error accepting new client: %s]\n", strerror(errno));
            exit(5);
        }

        User* user = new_user("*", "", inet_ntoa(client_address.sin_addr), 0, user_socket);

        pthread_mutex_lock(&users_mutex);
        add_user(users, user);
        pthread_mutex_unlock(&users_mutex);

        pthread_create(&(user->thread), NULL, (void * (*) (void *)) connect_user, user);
    }
}
